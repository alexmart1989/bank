#app, lib, ...

TEMPLATE = app
TARGET = bank
DESTDIR = ./bin
OBJECTS_DIR = ./.obj
CONFIG += c++11
QT -= core gui

INCLUDEPATH += src

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
        ./src/main.cpp \
        ./src/core/Account.cpp \
        ./src/core/Bank.cpp \
        ./src/core/Terminal.cpp \

HEADERS += \
        ./src/core/List.h \
        ./src/core/Account.h \
        ./src/core/Bank.h \
        ./src/core/ActionsBank.h \
        ./src/core/ActionsTerminal.h \
        ./src/core/Terminal.h \
