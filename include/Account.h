#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <iosfwd>

class Account
{
public:
  Account();
  Account(const Account& obj);
  Account operator=(const Account& obj);
  bool operator<(Account obj);
  bool operator>(Account obj);
  bool operator==(Account obj);
  friend std::ostream &operator<<(std::ostream &stream, const Account& obj);
  void addMoney(double depositMoney);
  bool takeMoney(double withdrawMoney);
  double getCashInfo() const;
private:
  double m_cash;
};

#endif
