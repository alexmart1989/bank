#ifndef ACTIONSBANK_H
#define ACTIONSBANK_H

enum ActionsBank
{
  actions_bank_first = 0,
  actions_bank_open_new_account,
  actions_bank_display_all_accounts,
  actions_bank_enter_account,
  actions_bank_close_account,
  actions_bank_exit,
  actions_bank_last,
};

#endif
