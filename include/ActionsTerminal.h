#ifndef ACTIONSTERMINAL_H
#define ACTIONSTERMINAL_H

enum ActionsTerminal
{
  FIRST,
  BALANCE,
  DEPOSIT,
  WITHDRAW,
  CLOSE,
  LAST,
};

#endif
