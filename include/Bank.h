#ifndef BANK_H
#define BANK_H

#include "List.h"
#include "Account.h"

class Account;

class Bank
{
public:
  Bank();
  void run();
private:
  void showMenu() const;
  int getChoice() const;
  int getIndex();

  List<Account> m_list;
};

#endif
