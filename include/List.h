#ifndef LIST_H
#define LIST_H

#include <iostream>

template <class T>
struct Node
{
  T data;
  Node<T>* next;

  Node(const T& input)
    : data(input),
    next(nullptr)
  {}
};

template <class T>
class List
{

public:

  List()
  {
    m_first = nullptr;
  }

  ~List()
  {
    while(m_first != nullptr)
    {
      Node<T>* victim = m_first;
      m_first = m_first->next;
      delete victim;
    }
    m_first = nullptr;
  }

  T& operator[](int index)
  {
    Node<T>* current = nullptr;
    if(index >= 0 && index < this->size())
    {
      int count = 0;
      current = m_first;
      while(current != nullptr && count != index)
      {
        current = current->next;
        count++;
      }
    }

    return current->data;
  }

  void push_front(T& input)
  {
    Node<T>* head = m_first;
    Node<T>* newNode = new Node<T>(input);
    m_first = newNode;
    m_first->next = head;
  }

  void push_back(T& input)
  {
    if(m_first == nullptr)
    {
      push_front(input);
    }
    else
    {
      Node<T>* current = m_first;
      while(current->next != nullptr)
      {
        current = current->next;
      }
      Node<T>* newNode = new Node<T>(input);
      current->next = newNode;
    }
  }

  void addByIndex(int index, T& input)
  {
    int indexMax = this->size() - 1;
    if(index == 0)
    {
      push_front(input);
    }
    else if(index == indexMax)
    {
      push_back(input);
    }
    else
    {
      int count = 1;
      Node<T>* previous = m_first;
      Node<T>* current = m_first;
      while(current != nullptr)
      {
        previous = current;
        current = current->next;

        if(index == count)
        {
          Node<T>* newNode = new Node<T>(input);
          previous->next = newNode;
          newNode->next = current;
          break;
        }

        count++;
      }
    }
  }

  void display()
  {
    Node<T>* current = m_first;
    while(current != nullptr)
    {
      std::cout << current->data << " ";
      current = current->next;
    }
    std::cout << "\n";
  }

  void clear()
  {
    while(m_first != nullptr)
    {
      Node<T>* victim = m_first;
      m_first = m_first->next;
      delete victim;
    }
  }
    
  int size()
  {
    int size = 0;
    Node<T>* current = m_first;
    while(current != nullptr)
    {
      size++;
      current = current->next;
    }
    return size;
  }

  void removeItem(int index)
  {
    if(index == 0)
    {
      Node<T>* temp = m_first;
      m_first = m_first->next;
      delete temp;
    }
    else
    {
      int count = 0;
      Node<T>* previous = m_first;
      Node<T>* current = m_first;
      while(current != nullptr)
      {
        previous = current;
        current = current->next;
        count++;

        if(index == count)
        {
          previous->next = current->next;
          delete current;
          break;
        }
      }
    }
  }

  void changeItemValue(int index, T& input)
  {
    int count = 0;
    Node<T>* current = m_first;
    while(current != nullptr)
    {
       if(count == index)
       {
          current->data = input;
          break;
       }
       current = current->next;
       count++;
    }
  }

  void reverse()
  {
    Node<T>* previous = nullptr;
    Node<T>* current = m_first;
    while(current != nullptr)
    {
      Node<T>* next = current->next;
      current->next = previous;
      previous = current;
      current = next;
    }
    m_first = previous;
  }


  void selectSort()
  {
    for(Node<T>* out = m_first; out->next != nullptr;)
    {
      Node<T>* min = out;
      for(Node<T>* in = out->next; in != nullptr; in = in->next)
      {
        if(in->data < min->data)
        {
            min = in;
        }
      }

      swap(out, min);
      out = min->next;

    }
  }

  void mergeSort()
  {
    int currentSize = this->size();
    m_first = startMergeSort(m_first, currentSize);
  }


private:

  Node<T>* startMergeSort(Node<T>* left, int size)
  {
    Node<T>* result = nullptr;
    if(size == 1)
    {
        result = left;
    }
    else
    {
      Node<T>* center = left;
      int middle = size / 2;
      int leftListSize = 0;
      while(leftListSize < middle)
      {
        center = center->next;
        ++leftListSize;
      }

      int rightListSize = size - leftListSize;

      Node<T>* leftSortedList = startMergeSort(left, leftListSize);
      Node<T>* rightSortedList = startMergeSort(center, rightListSize);

      result = merge(leftSortedList, leftListSize, rightSortedList, rightListSize);
    }

    return result;
  }

  Node<T>* merge(Node<T>* left, int leftSize, Node<T>* right, int rightSize)
  {
    // first condition
    Node<T>* firstElement = nullptr;
    if(left->data < right->data)
    {
      firstElement = left;
      left = left->next;
      leftSize--;
    }
    else
    {
      firstElement = right;
      right = right->next;
      rightSize--;
    }

    Node<T>* lastElement = firstElement;

    // intermediate conditions
    while(leftSize > 0 && rightSize > 0)
    {
      if(left->data < right->data)
      {
        lastElement->next = left;
        left = left->next;
        leftSize--;
      }
      else
      {
        lastElement->next = right;
        right = right->next;
        rightSize--;
      }

      lastElement = lastElement->next;
    }

    //last conditions
    while(leftSize > 0)
    {
      lastElement->next = left;
      lastElement = lastElement->next;
      left = left->next;
      leftSize--;
    }

    while(rightSize > 0)
    {
      lastElement->next = right;
      lastElement = lastElement->next;
      right = right->next;
      rightSize--;
    }

    lastElement->next = nullptr;

    return firstElement;

  }

  void swap(Node<T>* first, Node<T>* second)
  {
    if(first != second)
    {
      Node<T>* next_second = second->next;
      Node<T>* prev_second = findPrevious(second);
      Node<T>* prev_first = findPrevious(first);

      if(prev_first)
      {
        prev_first->next = second;
      }
      else
      {
        m_first = second;
      }

      if(prev_second)
      {
        prev_second->next = first;
      }
      else
      {
        m_first = first;
      }

      second->next = first->next;
      first->next = next_second;
    }
    else
    {
      return;
    }
  }

  Node<T>* findPrevious(Node<T>* node)
  {
    Node<T>* previous = nullptr;
    Node<T>* current = m_first;
    while(current != node)
    {
      previous = current;
      current = current->next;
    }

    return previous;

  }

  Node<T>* m_first;
};

#endif
