#ifndef TERMINAL_H
#define TERMINAL_H

#include "Account.h"

class Terminal
{
public:
    Terminal(Account& current);
    void run();
private:
    void deposit(double depositMoney);
    bool withdraw(double withdrawMoney);
    double getCashInfo() const;
    int getChoice() const;
    void showTerminalMenu() const;
    double enterMoneyValue() const;

    Account& m_account;
};

#endif
