#include "Account.h"

#include <iostream>

Account::Account():
    m_cash(0)
{}

Account::Account(const Account &obj)
{
    this->m_cash = obj.m_cash;
}

Account Account::operator=(const Account& obj)
{
    this->m_cash = obj.m_cash;
    return *this;
}

bool Account::operator<(Account obj)
{
    return (this->m_cash < obj.m_cash);
}

bool Account::operator>(Account obj)
{
    return (this->m_cash > obj.m_cash);
}

bool Account::operator==(const Account obj)
{
  return (this->m_cash - obj.m_cash) < 0.001;
}

double Account::getCashInfo() const
{
    return m_cash;
}

void Account::addMoney(double depositMoney)
{
    m_cash += depositMoney;
}

bool Account::takeMoney(double withdrawMoney)
{
    bool successful = false;
    if(m_cash > withdrawMoney)
    {
        m_cash -= withdrawMoney;
        successful = true;
    }
    
    return successful;
}

std::ostream &operator<<(std::ostream &stream, const Account& obj)
{
  stream << obj.getCashInfo() << " ";
  return stream;
}
