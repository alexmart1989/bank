#include "Bank.h"
#include "ActionsBank.h"
#include "Terminal.h"

#include <iosfwd>

Bank::Bank()
{}

void Bank::run()
{
    bool exit = false;
    while(!exit)
    {
        showMenu();
        int action = getChoice();
        switch(action)
        {
            case actions_bank_open_new_account:
            {
                Account newAccount;
                m_list.push_front(newAccount);
                std::cout << "New account was created!\n";
                break;
            }
            case actions_bank_display_all_accounts:
            {
                m_list.display();
                break;
            }
            case actions_bank_enter_account:
            {
                int index = getIndex();
                Terminal terminal(m_list[index]);
                terminal.run();
                break;
            }
            case actions_bank_close_account:
            {
                int index = getIndex();
                m_list.removeItem(index);
                std::cout << "Account with id: " << index << " was closed\n";
                break;
            }
            case actions_bank_exit:
            {
                exit = true;
                std::cout << "Good bye\n";
                break;
            }
            default:
            {
                std::cout << "Wrong input! Try again\n";
                break;
            }
        }

    }
}

void Bank::showMenu() const
{
    std::cout << "Welcome to Safe bank\n\n";
    std::cout << actions_bank_open_new_account << " - OPEN NEW ACCOUNT\n";
    std::cout << actions_bank_display_all_accounts << " - DISPLAY ALL ACCOUNTS\n";
    std::cout << actions_bank_enter_account << " - CHOOSE YOUR ACCOUNT\n";
    std::cout << actions_bank_close_account << " - CLOSE ACCOUNT\n";
    std::cout << actions_bank_exit << " - EXIT\n";
}

int Bank::getChoice() const
{
    int userChoice = 0;
    bool correct = false;
    while(!correct)
    {
        std::cout << "Please, select option ["
                  << actions_bank_open_new_account
                  << "-" << actions_bank_exit << "]\n";
        std::cin >> userChoice;

        if(userChoice > actions_bank_first && userChoice < actions_bank_last)
        {
            correct = true;
        }
        else
        {
            std::cout << "Wrong input! Try again\n";
        }
    }

    return userChoice;
}

int Bank::getIndex()
{
    int index = 0;
    bool correct = false;
    while(!correct)
    {
        std::cout << "Please, enter index: ";
        std::cin >> index;

        if((index >= 0) && (index < this->m_list.size()))
        {
            correct = true;
        }
        else
        {
            std::cout << "Index out of range. Enter again\n";
        }
    }

    return index;

}
