#include "Terminal.h"
#include "ActionsTerminal.h"

#include <iostream>

Terminal::Terminal(Account& current):
    m_account(current)
{}

void Terminal::deposit(double depositMoney)
{
    m_account.addMoney(depositMoney);
}

bool Terminal::withdraw(double withdrawMoney)
{
    bool successful = m_account.takeMoney(withdrawMoney);
    return successful;
}

void Terminal::run()
{
    std::cout << "You enter in you account" << std::endl;
    bool close = false;
    while(!close)
    {
        showTerminalMenu();
        int action = getChoice();
        switch(action)
        {
            
            case BALANCE:
            {
                double currentCash = m_account.getCashInfo();
                std::cout << "Current balance: " << currentCash << std::endl;
                break;
            }
            case DEPOSIT:
            {
                double amountMoney = enterMoneyValue();
                deposit(amountMoney);
                std::cout << "Cash was deposited\n";
                break;
            }
            case WITHDRAW:
            {
                double amountMoney = enterMoneyValue();
                bool successful = withdraw(amountMoney);
                if(successful)
                {
                    std::cout << "Cash was withdrawed\n";
                }
                else
                {
                    std::cout << "Not enough money! Please, check you balance and try again\n";
                }

                break;
            }
            case CLOSE:
            {
                close = true;
                std::cout << "Good bye\n";
                break;
            }
            default:
            {
                std::cout << "Wrong input! Try again\n";
                break;
            }   
        }
    }

}

void Terminal::showTerminalMenu() const
{
    std::cout << "\tTerminal Menu\n";
    std::cout << BALANCE << " - BALANCE\n";
    std::cout << DEPOSIT << " - DEPOSIT\n";
    std::cout << WITHDRAW << " - WITHDRAW\n";
    std::cout << CLOSE << " - CLOSE\n";
}

double Terminal::enterMoneyValue() const
{
    double money = 0;
    bool correct = false;
    while(!correct)
    {
        std::cout << "Please, enter amount money\n";
        std::cin >> money;
        if(money > 0)
        {
            correct = true;
        }
    }

    return money;

}

int Terminal::getChoice() const
{
    int userChoice = 0;
    std::cout << "Please, select option [" << BALANCE << "-" << CLOSE << "]:";
    std::cin >> userChoice;
    return userChoice;
}
